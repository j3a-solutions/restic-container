#!/bin/bash

set -eu

TIMEZONE=`date | cut -d' ' -f5`;

echo "It's now `date`"
echo "Scheduling daily backup at $DAILY_SCHEDULE $TIMEZONE"

while true;
do
    NOW=`date | cut -d' ' -f4`;
    if [[ $NOW == $DAILY_SCHEDULE ]]
    then
        $RESTIC_DIR/backup.sh;
        sleep 1s;
        echo "Next backup scheduled for `date -d $DAILY_SCHEDULE' today +1days'`"
    fi
done