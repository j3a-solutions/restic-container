#!/bin/sh

set -eu

if [ -z "${DAILY_SCHEDULE+x}" ]
then
    $RESTIC_DIR/backup.sh;
else
    $RESTIC_DIR/scheduler.sh;
fi