#!/bin/sh

set -eu

export RESTIC_REPOSITORY=s3:$S3_ENDPOINT/$BUCKET_NAME

echo "Checking snapshots and whether repository exists";
restic snapshots --cache-dir $RESTIC_DIR && REPO_EXISTS=$? || REPO_EXISTS=$?

if [ ! $REPO_EXISTS -eq 0 ]
then
  echo "Creating repository in $RESTIC_REPOSITORY if it does not exist"
  restic init;
fi

restic backup --cache-dir $RESTIC_DIR --verbose backup $BACKUP_DIR

if [ $ENABLE_PRUNE -eq 1 ]
then
  restic forget --prune --group-by paths --cache-dir $RESTIC_DIR --keep-last $KEEP_LAST;
fi