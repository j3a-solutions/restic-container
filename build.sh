#!/bin/sh

set -e

NAME=restic
TAG=$1
LATEST=j3asolutions/$NAME:latest;

echo "Building image $LATEST";

podman build \
  --pull \
  --tag "$LATEST" .;

podman push $LATEST;

if [[ $TAG != "" ]]; then
  VERSIONED_TAG=j3asolutions/$NAME:$TAG;
  echo "Tagging $VERSIONED_TAG";

  podman tag $LATEST $VERSIONED_TAG;
  podman push $VERSIONED_TAG;
fi