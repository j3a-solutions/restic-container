# Restic backups

Rootless container to perform a restic backup of a directory.

Since the container aims to run without `privileged` permissions, `crond` is not able to run.

Instead, a basic scheduler will run if a `DAILY_SCHEDULE` env variable is passed in the format HH:MM:SS. Otherwise, the container will complete once the backup restic command finishes.

## Environment variables

| Variable name         | Description                                                                                                                                      |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| S3_ENDPOINT           | Location of your S3 storage. Defaults to "https://vault.ecloud.co.uk"                                                                            |
| AWS_ACCESS_KEY_ID     | Key id for s3 storage. Required                                                                                                                  |
| AWS_SECRET_ACCESS_KEY | Key secret for s3 storage. Required                                                                                                              |
| BACKUP_DIR            | The directory to backup. Required                                                                                                                |
| BUCKET_NAME           | S3 bucket for your Restic repository. The bucket must exist, but if a Restic repository doesn't, this container will create it for you. Required |
| ENABLE_PRUNE          | Whether to forget and prune old snapshots. Value is either 0 or 1. Default is `1`                                                                |
| KEEP_LAST             | How many snapshots to keep. Defaults to 100                                                                                                      |
| RESTIC_PASSWORD       | Restic password env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                         |
| RESTIC_PASSWORD_FILE  | Restic password file env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                    |
| DAILY_SCHEDULE        | If set in the format HH:MM:SS, a scheduler will be the main process in the container                                                             |

## Example

```
docker run --rm \
  -e BUCKET_NAME=restic \
  -e AWS_ACCESS_KEY_ID=<my-key-id> \
  -e AWS_SECRET_ACCESS_KEY=<my-key-secret> \
  -e RESTIC_PASSWORD=<my-password> \
  -e BACKUP_DIR=/home/user \
  j3asolutions/restic
```
